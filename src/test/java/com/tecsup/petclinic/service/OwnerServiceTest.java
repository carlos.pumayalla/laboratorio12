package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecsup.petclinic.domain.Owner;
import com.tecsup.petclinic.domain.Pet;
import com.tecsup.petclinic.exception.OwnerNotFoundException;
import com.tecsup.petclinic.exception.PetNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class OwnerServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(OwnerServiceTest.class);

	@Autowired
   	OwnerService ownerService;

	/**
	 * 
	 */
	
	@Test
	public void testFindOwnerById() {

		long ID = 1;
		String NAME = "George";
		Owner owner = null;
		
		try {
			
			owner = ownerService.findById(ID);
			
		} catch (OwnerNotFoundException e) {
			fail(e.getMessage());
		}
		logger.info("" + owner);

		assertEquals(NAME, owner.getFirst_name());

	}
	/*
	@Test
	public void testFindOwnerByName() {
		String FIND_NAME = "George";
		int SIZE_EXPECTED = 1;

		List<Owner> owners = ownerService.findByName(FIND_NAME);

		assertEquals(SIZE_EXPECTED, owners.size());
	}*/
	
	@Test
	public void testCreateOwner() {

		String FIRST_N = "Carlos";
		String LAST_N = "Pumayalla";
		String ADDRESS = "Florencia de Mora";
		String CITY = "Trujillo";
		String TELEPHONE = "962172839";

		Owner owner = new Owner(FIRST_N, LAST_N, ADDRESS, CITY, TELEPHONE);
		owner = ownerService.create(owner);
		logger.info("" + owner);

		assertThat(owner.getId()).isNotNull();
		assertEquals(FIRST_N, owner.getFirst_name());
		assertEquals(LAST_N, owner.getLast_name());
		assertEquals(ADDRESS, owner.getAddress());
		assertEquals(CITY, owner.getCity());
		assertEquals(TELEPHONE, owner.getTelephone());

	}
	
	@Test
	public void testUpdateOwner() {

		String FIRST_N = "Eduardo";
		String LAST_N = "Angeles";
		String ADDRESS = "Porvenir";
		String CITY = "Trux";
		String TELEPHONE = "996634563";
		long create_id = -1;

		String UP_FIRST_N = "Edu";
		String UP_LAST_N = "AngeL";
		String UP_ADDRESS = "Esperanza";
		String UP_CITY = "Trujillo";
		String UP_TELEPHONE = "996633333";

		Owner owner = new Owner(FIRST_N, LAST_N, ADDRESS, CITY, TELEPHONE);

		// Create record
		logger.info(">" + owner);
		Owner readOwner = ownerService.create(owner);
		logger.info(">>" + readOwner);

		create_id = readOwner.getId();

		// Prepare data for update
		readOwner.setFirst_name(UP_FIRST_N);
		readOwner.setLast_name(UP_LAST_N);
		readOwner.setAddress(UP_ADDRESS);
		readOwner.setCity(UP_CITY);
		readOwner.setTelephone(UP_TELEPHONE);
		// Execute update
		Owner upgradeOwner = ownerService.update(readOwner);
		logger.info(">>>>" + upgradeOwner);

		assertThat(create_id).isNotNull();
		assertEquals(create_id, upgradeOwner.getId());
		assertEquals(UP_FIRST_N, upgradeOwner.getFirst_name());
		assertEquals(UP_LAST_N, upgradeOwner.getLast_name());
		assertEquals(UP_ADDRESS, upgradeOwner.getAddress());
		assertEquals(UP_CITY, upgradeOwner.getCity());
		assertEquals(UP_TELEPHONE, upgradeOwner.getTelephone());
	}
	
	@Test
	public void testDeleteOwner() {

		String FIRST_N = "Car";
		String LAST_N = "Ange";
		String ADDRESS = "Porv";
		String CITY = "Trux";
		String TELEPHONE = "996699999";

		Owner owner = new Owner(FIRST_N, LAST_N, ADDRESS, CITY, TELEPHONE);
		owner = ownerService.create(owner);
		logger.info("" + owner);
		
		try {
			ownerService.delete(owner.getId());
		} catch (OwnerNotFoundException e) {
			fail(e.getMessage());
		}
			
		try {
			ownerService.findById(owner.getId());
			assertTrue(false);
		} catch (OwnerNotFoundException e) {
			assertTrue(true);
		} 				

	}
	
}